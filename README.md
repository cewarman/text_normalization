# 中英夾雜文字正規化系統 CEmix text normalization system
這個項目只是利用120.126.151.132的文字正規劃網頁服務跟Python HTTP庫裡的requests，在終端機上用指令實現文字正規化功能。
This project just uses the text-normalization service from 120.126.151.132 and requests in the Python HTTP library to implement text normalization on the terminal.

用法(usage)：
```
$ python3 reqtest.py testin.txt
```
